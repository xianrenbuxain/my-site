
import instance from './instance'

// 获取所有博客分类
export async function getBlogtype() {
  const res = await instance.get('/api/blogtype')
  return res.data
}

// 分页获取所有博客

export async function getBlog(page = 1, limit = 10, categoryid = -1, keyword = '') {
  const res = await instance.get('/api/blog', {
    params: {
      page,
      limit,
      categoryid,
      keyword
    }
  })
  return res.data
}

// 获取单个博客页详情
export async function getBlogDetails(id) {
  const res = await instance.get(`/api/blog/${id}`)
  return res.data
}


// 提交评论
export async function postComment(commentInfo) {
  const res = await instance.post('/api/comment', commentInfo)
  return res.data
}

//分页获取评论
export async function getComment(blogid, page = 1, limit = 10, keyword = '') {
  const res = await instance.get('/api/comment', {
    params: {
      blogid,
      page,
      limit,
      keyword
    }
  })
  return res.data
}