import instance from './instance'

export default async function getBanner() {
  const res = await instance.get('/api/banner')
  return res.data
}