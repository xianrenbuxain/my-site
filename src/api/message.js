
import instance from './instance'


// 提交留言
export async function postMessage(messageInfo) {
  const res = await instance.post('/api/message', messageInfo)
  return res.data
}

export async function getMessage(page = 1, limit = 10, keyword = '') {
  const res = await instance.get('/api/message', {
    params: {
      page,
      limit,
      keyword
    }
  })
  return res.data
}