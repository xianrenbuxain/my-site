import instance from './instance'

export default async function getProject() {
  const res = await instance.get('/api/project')
  return res.data
}