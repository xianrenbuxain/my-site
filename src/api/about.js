import instance from './instance'

export default async function getAbout() {
  const res = await instance.get('/api/about')
  return res.data
}