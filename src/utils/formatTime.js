export function formatTimestamp(timestamp, hasSeconds = false) {
  const date = new Date(timestamp);
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, '0'); // 月份从0开始，需要加1
  const day = String(date.getDate()).padStart(2, '0');
  let ret = `${year}-${month}-${day}`;
  if (hasSeconds) {
    const hour = String(date.getHours()).padStart(2, '0')
    const minutes = String(date.getMinutes()).padStart(2, '0')
    const seconds = String(date.getSeconds()).padStart(2, '0')
    ret += ' ' + hour + ':' + minutes + ':' + seconds
  }

  return ret
}