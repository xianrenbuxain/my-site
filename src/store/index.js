import Vuex from 'vuex';
import Vue from 'vue';
import banner from './banner'
import about from './about'
import project from './project';
Vue.use(Vuex); // 应用vuex插件
const store = new Vuex.Store({
  modules: {
    banner,
    about,
    project,

  }
});


window.store = store
export default store;