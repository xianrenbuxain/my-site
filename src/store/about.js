import getAbout from "@/api/about";


const about = {
  namespaced: true,
  state: () => ({
    isLoading: false,
    src: '',
  }),
  mutations: {
    setIsLoading(state, status) {
      state.isLoading = status
    },
    setSrc(state, src) {
      state.src = src
    }
  },
  actions: {
    async about(context) {
      if (context.state.src) {
        return;
      }
      context.commit('setIsLoading', true)
      const resp = await getAbout()
      context.commit('setSrc', resp.data)
      context.commit('setIsLoading', false)
    }
  },
}


export default about;