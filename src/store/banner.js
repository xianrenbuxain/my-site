import getBanner from "@/api/banner"

const banner = {
  namespaced: true,
  state: () => ({
    isLoading: false,
    bannerList: [],
  }),
  mutations: {
    setIsLoading(state, status) {
      state.isLoading = status
    },
    setBannerList(state, data) {
      state.bannerList = data
    }
  },
  actions: {
    async banner(context) {
      if (context.state.bannerList.length > 0) {
        return;
      }
      context.commit('setIsLoading', true)
      const resp = await getBanner()
      context.commit('setBannerList', resp.data)
      context.commit('setIsLoading', false)
    }
  },
  getters: {
    bannerListLen(state) {
      return state.bannerList.length
    }
  }
}


export default banner;