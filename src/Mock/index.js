import Mock from 'mockjs';

import './blog.js';
import './banner.js';
import './about.js';
import './project.js'
import './message.js'

Mock.setup({
  timeout: '1000-2000'
})

