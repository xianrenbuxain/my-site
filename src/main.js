// 入口文件
import Vue from 'vue'
import App from './App.vue'
// 全局注册统一样式
import './styles/global.less';

import router from './router'
import store from './store';

// 运行一遍mock
import './Mock'

import { showMessage } from './utils'
import './eventBus'; //事件总线

import loading from '@/directives/loading.js'
import lazy from '@/directives/lazy.js'

Vue.directive('loading', loading) //全局定义自定义组件
Vue.directive('lazy', lazy) //全局定义 懒加载

Vue.prototype.$showMessage = showMessage

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
