import loading from '@/assets/loading.svg';
import style from './loading.module.less'

function createImg() {
  const img = document.createElement('img');
  img.src = loading
  img.className = style['loading-container']
  img.dataset.loading = 'loading'
  return img
}

function checkImg(el) {
  const imgs = el.querySelectorAll('img[data-loading="loading"]')
  return imgs
}

// 在bingding.value改变时，删除或者增加元素
// true加，false删
export default function (el, bingding) {
  let moveImgs = Array.from(checkImg(el))
  moveImgs = moveImgs.length === 0 ? false : moveImgs
  if (!bingding.value) {
    // 删除
    if (moveImgs) {
      moveImgs.forEach(img => {
        img.remove()
      })
    }
  } else {
    // 增加
    if (moveImgs) {
      return;
    }
    const img = createImg()
    el.appendChild(img);
  }
}