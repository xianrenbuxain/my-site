const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/api': {
        // 当请求地址以 api 开头时，代理到另一个地址
        target: 'https://study.duyiedu.com', // 代理的目标地址
        changeOrigin: true, // 更改请求头中的host，无须深究，为避免出问题，最好写上
      },
    },
  },
})
